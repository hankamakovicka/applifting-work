# STFUNDCLICK.COM

## O projektu
Tato  aplikace je vytvořena v Reactu. 
Verze projektu: 1.0
Aplikaci si můžete vyzkoušet: [React App](https://cryptic-cove-43861.herokuapp.com/Applifting)

Jedná se o klikací hru, kdy máte na úvodní obrazovce možnost vytvořit si vlastní tým zadáním jména týmu do formuláře a nebo pomoci jinému týmu k lepšímu výsledku kliknutím na daný tým. Další možnost jak pomoct jinému týmu, je napsání jména daného týmu do formuláře. Na úvodní obrazovce najdete také seznam 10 nejlepších týmů, který můžete brát jako motivaci pro vaše zapojení do hry.

Poté, co si vytvoříte, nebo vyberete daný tým zobrazí se vám již okno daného týmu a vy se můžete pustit do "zběsilého" klikání. Zobrazují se jak vaše kliky, tak celkový počet kliků všech hráčů, kteří do daného týmu svým klikáním přispěli. Dále můžete vidět jak se posunujete v tabulce všech týmů.

Vrátit na úvodní obrazovku se můžete pomocí odkazu STFUNDCLICK.COM v horní části obrazovky.

## Rozjetí projektu
Po stažení projektu do počítače je třeba spustit příkaz `npm install`. Až se vám vše nainstaluje, můžete příkazem `npm start` spustit aplikaci.

## Shrnutí projektu
Jedná se o moji první aplikaci napsanou v Reactu. Asi to půjde poznat, že je první. Ale i tak si myslím, že jsem se naučila hodně věcí v poměrně krátkém čase, který jsem této aplikaci mohla věnovat. React mě opravdu nadchnul a určitě se v něm chci dál rozvíjet. 
Věc, kterou považuji jako chybu této aplikace je to, že na ni není použitý less, popřípadě sass. Práci s preprocesory ovládám, ale nedokázala jsem je zakomponovat do Reactu.
Na druhou stranu, jako pozitivum vnímám to, že mi hra funguje, tak jak má (za předpokladu, že jsem správně pochopila zadání).

## Na závěr
Nezbývá mi nic jiné, než vám popřát příjemné hraní.

# STFUNDCLICK.COM - EN

## About the project
This app is created in React.
Project version: 1.0
You can try the app: [React App](https://cryptic-cove-43861.herokuapp.com/Applifting)

This is a clickable game where you can create your own team by entering your team name on the form / or helping another team to get a better result by clicking on the team. Another way to help another team is to write the team name on the form. On the opening screen you will also find a list of the top 10 teams that you can take as a motivation for your involvement in the game.

Once you have created or selected a team, the team's window will appear and you will be able to click. You will see both, your clicks and the total number of clicks, from all players who have contributed to that team. In addition, you can see how you are moving in the table of all teams.

You can return to the home screen using the STFUNDCLICK.COM link at the top of the screen.

## Launching the project
You must run the `npm install` command after you download the project to your computer. Once everything is installed, you can use the `npm start` command to run the application.

## Project summary
This is my first app written in React. I guess it'll see that this is the first. But even so, I think I learned a lot of things in a relatively short time I could devote to this app. React really excited me and I definitely want to develop in it.
The thing I consider to be a bug of this application is that less or sass is not used. I work with preprocessors, but I couldn't incorporate them into React.
On the other hand, as a positive, I feel that the game works as it should (assuming I have correctly understood the assignment).

## Finally
I have nothing else but to wish you enjoy playing.