import React, { Component } from 'react';
import './Motto.css';

class Motto extends Component {
    render() {
        return (
            <div className="App-motto">
                <p>" It's really simple, you just need click as fast you can. "</p>
                <p className="App-motto-autor">- anonymus</p>
            </div>
        );
    }
}

export default Motto;