import React from 'react';
import './Best.css';
import classNames from 'classnames';
import {
  string,
  arrayOf,
  shape,
  number,
  func,
} from 'prop-types';

class Best extends React.PureComponent {
  static propTypes = {
    items: arrayOf(shape({
      team: string.isRequired,
      click: number.isRequired,
    })),
    activeTeam: shape({
      team: string.isRequired,
      click: number.isRequired,
    }),
    onClick: func.isRequired,
  }

  static defaultProps = {
    items: [],
    activeTeam: null,
  }

  render () {
    const {
      items,
      activeTeam,
      onClick,
      className
    } = this.props;
    return (
      console.log(className),
      <div className={ className }>
        <h4 className="headline-left">Team</h4>
        <h4 className="headline-right">Clicks</h4>
        <ol>
        {
          items
            .map((item, index) => {
              const {
                team: teamName,
                click,
              } = item;

            return (
              <li
                key={teamName}
                onClick={(e) => onClick(e, item)}
                className={classNames(
                  {
                    'active': teamName === (activeTeam && activeTeam.team),
                  }
                )}
              >
                {teamName}
                <span className="click">{click}</span>
              </li>
            );
          })
        }
      </ol>
      </div>
    );
  }
}

export default Best;