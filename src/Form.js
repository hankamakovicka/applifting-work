import React, { Component } from 'react';
import './Form.css';

class Form extends Component {

  constructor(props) {
    super();
    this.state = {
      term : props.term
    }
  }

  render() {
      return (
          <div className="App-form">
              <form onSubmit={this.state.onSubmit}>
                  <label>Napis jmeno tymu</label>
                      <input type="text" name="name" placeholder="Team" value={this.state.term} onChange={this.onChange.bind(this)} />
                      <input type="submit" value="Click" />
              </form>
          </div>
      );
  }
}

export default Form;

