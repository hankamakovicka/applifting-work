import React from 'react';
import './Detail.css';

class Detail extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        clicks: 0,
      };
      this.activeTeam = null;
    }

    ClickItem = () => {
        this.setState({
          clicks: this.state.clicks + 1,
        });
        this.props.onScoreUpdate(this.props.activeTeam);
    }

    componentDidUpdate(prevProps, prevState) {
      const currentTeam = this.props.activeTeam || {};
      const prevTeam = prevProps.activeTeam || {};
      if (currentTeam.team !== prevTeam.team) {
        this.setState({ clicks: 0 });
      }
    }

    render() {
        return (
          <div>
            <div className="button-area">
              <button onClick={this.ClickItem}>Click!</button>
            </div>
            <div className="counter-area">
              <div className="item">
                <h3>Your clicks:</h3>
                <p>{this.state.clicks}</p>
              </div>
              <div className="item">
                <h3>Team clicks:</h3>
                <p>{this.props.activeTeam ? this.props.activeTeam.click : 0}</p>
              </div>
            </div>
          </div>
        );

      }
  }
  export default Detail;