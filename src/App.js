import React, { Component } from 'react';
import Motto from './Motto';
// import Form from './Form';
import Best from './Best';
import Detail from './Detail';
import './App.css';
import './Form.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: '',
      items: [],
      teamClicks: [
        {team : 'Applifting', click: 1002345},
        {team : 'Filip', click: 996032},
        {team : 'Mlask', click: 96843},
        {team : 'Jakub', click: 82552},
        {team : 'Prokop', click: 65535},
        {team : 'Zuzka', click: 60223},
        {team : 'Vráťa', click: 25005},
        {team : 'Martin', click: 22152},
        {team : 'Borec', click: 13123},
        {team : 'CSP', click: 5},
        {team : 'Posledňáček', click: 1},
      ],
      activeTeam: null,
      page: 'index',
    };
  }

  onChange = (event) => {
    this.setState({term: event.target.value});
  }

  onClick = (event, item) => {
    this.setActiveTeam(item);
  }

  onScoreUpdate = (team) => {
    const newTeam = {
      ...team,
      click: team.click + 1
    };
    this.state.teamClicks[this.state.teamClicks.indexOf(team)] = newTeam;
    this.setState({
      teamClicks: [...this.state.teamClicks],
      activeTeam: newTeam,
    });
  }

  onSubmit = (event) => {
    event.preventDefault();
    if (this.state.term === '' || this.state.term.length < 3) {
      return;
    }

    const team = this.state.teamClicks.find((item) => item.team.toLocaleLowerCase() === this.state.term.toLocaleLowerCase());

    if (team) {
      this.setActiveTeam(team);
      return;
    }

    const newTeam = {
      team: this.state.term,
      click: 0,
    };

    this.setState({
      term: '',
      teamClicks: this.state.teamClicks.concat(newTeam)
    });
    this.setActiveTeam(newTeam);
  }

  setActiveTeam(team){
    this.setState({
      activeTeam: team,
      page: 'team',
    });
    window.history.replaceState(null, team.team, `/${team.team}`);
  }

  goToIndex = (e) => {
    e.preventDefault();
    this.setState({
      page: 'index',
    });
    window.history.replaceState(null, 'index', '/');
  }

  render() {
    const sortedTeams = this.state.teamClicks.sort((a, b) => {
      if(a.click < b.click) {
        return 1;
      } else if(a.click > b.click) {
        return -1;
      }
      return 0;
    });

    // const activeTeam = this.state.teamClicks.find((currentTeam) => currentTeam.team === this.state.activeTeamName);


    return (
      <div>
        <header className="App-header">
          <h1><a href="/" onClick={this.goToIndex}>stfundclick.com</a></h1>
        </header>
        {this.state.page === 'index' && (
          <React.Fragment>
            <Motto />
            <main>
              <div className="App-main">
                {/* <Form onSubmit={this.setState.onSubmit} onChange={this.setState.onChange} value={this.state.term}/> */}
                <div className="App-form">
                    <form onSubmit={this.onSubmit}>
                        <label>Enter your team name:</label>
                        <input type="text" name="name" placeholder="Your mom" value={this.state.term} onChange={this.onChange} />
                        <input type="submit" value="Click" />
                    </form>
                </div>
                <div className="topten">
                  <div className="triangel">
                    <h2>TOP 10 Clickers</h2>
                  </div>
                </div>
                <Best items={sortedTeams.slice(0, 10)} onClick={this.onClick} />
                <p>Want to be top? STFU and click!</p>
              </div>
            </main>
          </React.Fragment>
        )}

        {this.state.page === 'team' && (
          <React.Fragment>
            <h2 className="headline-big">Clicking for team <strong>{this.state.activeTeam && this.state.activeTeam.team}</strong></h2>
            <div className="App-motto">
                <p>Too lazy to click? Let you pals click for you: <span className="url">stfundclick.com/{this.state.activeTeam && this.state.activeTeam.team}</span></p>
            </div>
            <main>
              <div className="App-main">
                <Detail activeTeam={this.state.activeTeam} onScoreUpdate={this.onScoreUpdate} />
                <Best className="best-detail"
                  items={sortedTeams}
                  activeTeam={this.state.activeTeam}
                  onClick={this.onClick}
                />
                <p>Want to be top? STFU and click!</p>
              </div>
            </main>
          </React.Fragment>
        )}
        <footer>
          <p>If you don't like this page, It's <a href="https://www.applifting.cz/" target="_blank">Applifting</a>'s fault.</p>
        </footer>
      </div>
    );
  }
}

export default App;


// TODO: omezit aby sa nedal vytvorit novy tim ked nie je nazov
// ak uz tim existuje tak sa nanho prepnut
// kazdy tim ma vlastnu naklikanu hodnotu a poradie podla toho kolko ma klikov
// ked sa klikne na tim tak sa presmeruje na timovu stranku kde sa moze klikat
// top 10 clickers

// TODO: detail: evidovat tvoje kliky pre dany tim, evidovat vsetky kliky za tim